from __future__ import with_statement
from fabric.contrib import console 
from fabric.api import *


@with_settings(warn_only=True)
@hosts("webapps@facemaps.in4.com.br")
def deploy():
    with cd('/var/webapps/facemaps/facemaps'):
        run('git pull')
        if console.confirm("Install requirements.txt?", default=False):
            run('../bin/pip install -r requirements.txt')
        if console.confirm("Run migrations?", default=False):
            run('../bin/python manage.py migrate')
        if console.confirm("Run collectstatic?", default=False):
            run('../bin/python manage.py collectstatic --noinput')
        run('supervisorctl restart facemaps')