# Django
django<2.2

# Deploy
pymysql
gunicorn
mysqlclient

# Images
pillow==5.2.0

# Rest
djangorestframework==3.8.2
django-rest-swagger==2.2.0