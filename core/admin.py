from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from core.models import Post, Comentario, Voto


class ComentarioInline(admin.TabularInline):
	model = Comentario
	extra = 1
	fields = ('usuario', 'conteudo', )

class VotoInline(admin.TabularInline):
	model = Voto
	extra = 1
	fields = ('usuario', 'resolvido', )

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
	list_display = ('titulo', 'data_post', 'quant_votos', 'usuario', 'slug', )
	list_fielter = ('usuario', 'data_post', 'quant_votos', )
	inlines = [ComentarioInline, VotoInline, ]
	fieldsets = (
        ('Post', {
            'fields': ('usuario', 'titulo', 'descricao', )

        }),
         ('Localização', {
            'fields': ('latitude', 'longitude', )
        }),
        ('Imagem', {
            'fields': ('imagem', )
        }),
    )

@admin.register(Comentario)
class ComentarioAdmin(admin.ModelAdmin):
	list_display = ('conteudo', 'data_comentario', 'usuario', 'post', )
	list_fielter = ('usuario', 'post', 'data_comentario', )
	fields = ('usuario', 'post', 'conteudo', )

@admin.register(Voto)
class VotoAdmin(admin.ModelAdmin):
	list_display = ('usuario', 'post', 'resolvido', )
	fields = ('usuario', 'post', 'resolvido', )
