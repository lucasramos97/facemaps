# -*- coding: utf-8 -*-
from django.urls import path, include

from rest_framework.authtoken import views as auth_views
from rest_framework import routers

from core import views

router = routers.SimpleRouter()
router.register(r'posts', views.PostViewSet)
# router.register(r'numeros', views.NumeroViewSet)

urlpatterns = [
    path('auth/token', auth_views.ObtainAuthToken.as_view()),
    path('auth/signup', views.CreateUserView.as_view()),
] + router.urls