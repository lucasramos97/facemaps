from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import pre_save, post_save, post_delete
from django.utils.text import slugify
from django.dispatch import receiver


class Post(models.Model):

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'

    slug = models.SlugField(u'Slug', max_length=50, unique=True, blank=True)
    titulo = models.CharField(u'Título', max_length=50)
    descricao = models.TextField(u'Descrição', max_length=75)
    data_post = models.DateTimeField(u'Data do Post', auto_now_add=True)
    latitude = models.DecimalField(u'Latitude', max_digits=20, decimal_places=14)
    longitude = models.DecimalField(u'Longitude', max_digits=20, decimal_places=14)
    imagem = models.ImageField(u'Imagem', upload_to='posts/')
    quant_votos = models.PositiveIntegerField(u'Quantidade de Votos', default=0)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.titulo


@receiver(pre_save, sender=Post)
def post_slug_pre_save(sender, instance, raw, using, update_fields, **kwargs):
    if not instance.slug:
        new_slug = slugify(instance.titulo)
        instance.slug = new_slug
        num = 1
        while Post.objects.filter(slug=instance.slug).exists():
            instance.slug = u'%s-%s' % (new_slug, num)
            num += 1

@receiver(post_delete, sender=Post)
def post_imagem_post_delete(sender, instance, *args, **kwargs):
    try:
        instance.imagem.delete()
    except:
        pass

class Comentario(models.Model):

    class Meta:
        verbose_name = 'Comentário'
        verbose_name_plural = 'Comentários'

    conteudo = models.CharField(u'Comentário', max_length=150)
    data_comentario = models.DateTimeField(u'Data do Comentário', auto_now_add=True)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __str__(self):
        return '%s: %s em %s.' %(self.usuario, self.conteudo, self.post)


class Voto(models.Model):

    class Meta:
        verbose_name = 'Voto'
        verbose_name_plural = 'Votos'

    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    resolvido = models.BooleanField(u'Resolvido', default=False)

    def save(self, *args, **kwargs):
        if Voto.objects.filter(post=self.post, usuario=self.usuario).exists():
            return
        else:
            super().save(*args, **kwargs)

@receiver(post_save, sender=Voto)
def voto_quant_votos_post_save(sender, instance, raw, using, update_fields, **kwargs):
    instance.post.quant_votos = len(sender.objects.filter(post=instance.post, resolvido=True))
    instance.post.save()