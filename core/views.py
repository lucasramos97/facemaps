from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.generics import GenericAPIView
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import PermissionDenied

from core import serializers
from core.models import Post, Comentario, Voto



class CreateUserView(GenericAPIView):
    serializer_class = serializers.UserSerializer
    permission_classes = ()
    
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key })



class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = serializers.PostSerializer

    def perform_create(self, serializer):
        serializer.save(usuario=self.request.user)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.usuario != request.user:
            raise PermissionDenied()
        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.usuario != request.user:
            raise PermissionDenied()
        return super().destroy(request, *args, **kwargs)

    @action(detail=True, methods=['get'])
    def comentarios(self, request, pk=None):
        instance = self.get_object()
        serializer = serializers.ComentarioSerializer(instance.comentario_set.all(), many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['get'])
    def votos(self, request, pk=None):
        instance = self.get_object()
        serializer = serializers.VotoSerializer(instance.voto_set.all(), many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['post'], serializer_class=serializers.ComentarioSerializer)
    def add_comentario(self, request, pk=None):
        instance = self.get_object()
        serializer = serializers.ComentarioSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(usuario=request.user, post=instance)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @action(detail=True, methods=['post'], serializer_class=serializers.ComentarioSerializer)
    def add_voto(self, request, pk=None):
        instance = self.get_object()
        serializer = serializers.VotoSerializer(data=request.data, context={'post': instance, 'usuario': request.user, })
        serializer.is_valid(raise_exception=True)
        serializer.save(usuario=request.user, post=instance)
        return Response(serializer.data, status=status.HTTP_201_CREATED)