# -*- coding: utf-8 -*-
from rest_framework import serializers
from django.contrib.auth.models import User

from core.models import Post, Comentario, Voto


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'password', )

    password = serializers.CharField(write_only=True)

    def save(self):
        credentials = {
            'username': self.validated_data.get('username'),
            'password': self.validated_data.get('password'),
        }

        if all(credentials.values()):
            # Try create new user
            try:
                user = User.objects.create_user(**credentials)
            except IntegrityError:
                raise serializers.ValidationError(u'Já existe uma conta com este email.', code='login_failed')
            except Exception as exc:
                raise serializers.ValidationError(u'%s' % exc, code='login_failed')
            if user:
                user.is_active = True
                user.first_name = self.validated_data.get('first_name')
                user.last_name = self.validated_data.get('last_name')
                user.save()
                return user
            else:
                msg = u'Não é possível fazer o login com as credenciais fornecidas.'
                raise serializers.ValidationError(msg, code='login_failed')
        else:
            msg = u'Os campos username e password são obrigatórios.'
            raise serializers.ValidationError(msg, code='login_failed')



class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ('id', 'titulo', 'descricao', 'latitude', 'longitude', 'imagem', 'data_post', 'quant_votos', 'usuario',  )
        read_only_fields = ('id', 'data_post', 'quant_votos', ) 

    usuario = UserSerializer(read_only=True, many=False)


class ComentarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comentario
        fields = ('id', 'conteudo', 'data_comentario', 'usuario', )
        read_only_fields = ('id', 'data_comentario', ) 
        
    usuario = UserSerializer(read_only=True, many=False)


class VotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Voto
        fields = ('id', 'usuario', 'post', 'resolvido', )
        read_only_fields = ('id', 'post', ) 

    usuario = UserSerializer(read_only=True, many=False)

    def save(self, **kwargs):
        try: self.instance = Voto.objects.get(post=self.context.get('post'), usuario=self.context.get('usuario'))
        except Voto.DoesNotExist: pass
        return super().save(**kwargs)
