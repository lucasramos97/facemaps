# Generated by Django 2.1.1 on 2018-09-25 14:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20180925_1013'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comentario',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='voto',
            name='slug',
        ),
    ]
